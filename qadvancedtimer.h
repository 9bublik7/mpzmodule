#ifndef QADVANCERDTIMER_H
#define QADVANCERDTIMER_H

#include <QTimer>

class QAdvancedTimer : public QTimer
{
public:
    QAdvancedTimer();
    void pause();
    void resume();
private:
    int curTime;
};

#endif // QADVANCERDTIMER_H
