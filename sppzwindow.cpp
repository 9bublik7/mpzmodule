#include "sppzwindow.h"
#include "ui_sppzwindow.h"
#include "toolswidget.h"

SPPZWindow::SPPZWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SPPZWindow)
{
    ui->setupUi(this);
    connect(&commonTime, SIGNAL(timeout()), this, SLOT(commonTimeIsOut()));
    StartWizard wiz(this);
    wiz.exec();
    setUpdating();
}

SPPZWindow::~SPPZWindow()
{
    delete ui;
}

void SPPZWindow::on_toolsButton_clicked()
{
    static bool time = false;
    const bool FIRST_TIME = false;
    const bool SECOND_TIME = true;

    clear(ui->informationField);

    switch(time)    {
    case FIRST_TIME:
        show(new ToolsWidget(this));
        break;
    case SECOND_TIME:
        QLabel *label = new QLabel("Информационное окно\nСППЗ");
        label->setFont(QFont("Sans Serif", 33));
        label->setAlignment(Qt::AlignVCenter| Qt::AlignHCenter);
        show(label);
        break;
    }
    time = time ^ true;
}

void SPPZWindow::clear(QWidget *widget)  {
    QLayoutItem *child;
    while ((child = widget->layout()->takeAt(0)) != 0) {
        delete child->widget();
        delete child;
    }
    delete widget->layout()->widget();
    delete widget->layout();
}

void SPPZWindow::show(QWidget *widget)  {
    QGridLayout *layout = new QGridLayout(NULL);
    widget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    layout->addWidget(widget);
    ui->informationField->setLayout(layout);
}

void SPPZWindow::on_pauseButton_clicked()
{
    if(commonTime.isActive())   {
        commonTime.pause();
    }
    else {
        commonTime.resume();
    }
    changePauseIcon();
}

void SPPZWindow::changePauseIcon()   {
    static bool time = false;
    const bool FIRST_TIME = false;
    const bool SECOND_TIME = true;
    switch(time)    {
    case FIRST_TIME:
        ui->pauseButton->setIcon(QIcon(":/images/play.png"));
        break;
    case SECOND_TIME:
        ui->pauseButton->setIcon(QIcon(":/images/pause.png"));
        break;
    }
    time = time ^ true;
}

void SPPZWindow::setUpdating()  {
    const int RUN_PERIOD_IN_MSEC = 100;
    connect(&updateTime, SIGNAL(timeout()), this, SLOT(updateTimeLabels()));
    updateTime.start(RUN_PERIOD_IN_MSEC);
}

void SPPZWindow::updateTimeLabels() {
    if(!commonTime.isActive()) return;
    int time = commonTime.remainingTime()/1000;
    QString resString;
    resString = resString.sprintf("%02d:%02d", time/60, (time%60));
    ui->commonTimeLabel->setText(resString);
}

void SPPZWindow::commonTimeIsOut()  {
    ui->pauseButton->setEnabled(false);
    QMessageBox::information(this, "Сообщение", "Время вышло");
}

void SPPZWindow::on_startButton_clicked()
{
    if(!commonTime.isActive())  {
        commonTime.setSingleShot(true);
        commonTime.start(1*5*1000);
        ui->pauseButton->setEnabled(true);
    }
}

void SPPZWindow::closeEvent(QCloseEvent *event) {
    commonTime.stop();
    if(event==NULL)
        return;
    parentWidget()->show();
}
