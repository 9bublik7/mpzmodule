#include "startwizard.h"

StartWizard::StartWizard(QWidget* parent)
    :QWizard(parent)
{
    QWizardPage* page = createPage();
    addPage(page);
}

QWizardPage* StartWizard::createPage()   {
    QWizardPage* page = new QWizardPage(this);
    page->setTitle(tr("Выбор занятия"));
    page->setSubTitle(tr("Выберите дисциплину, занятие и группу"));

    QBoxLayout* mainLayout = new QVBoxLayout;

    mainLayout->addWidget(disciplineLabel = new QLabel("Выберите дисциплину"));
    mainLayout->addWidget(disciplineEdit = new QComboBox);
    mainLayout->addWidget(lessonLabel = new QLabel("Выберите занятие"));
    mainLayout->addWidget(lessonEdit = new QComboBox);
    mainLayout->addWidget(groupLabel = new QLabel("Выберите группу"));
    mainLayout->addWidget(groupEdit = new QComboBox);
    page->setLayout(mainLayout);
    return page;
}
