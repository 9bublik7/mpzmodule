#include "authdialog.h"
#include "ui_authdialog.h"

QString AuthDialog::accountGroup = "";

AuthDialog::AuthDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthDialog)
{
    ui->setupUi(this);
}

AuthDialog::~AuthDialog()
{
    delete win;
    delete ui;
}

void AuthDialog::on_enterButton_clicked()
{
    if(!isFilled(ui->loginEdit)) {
        QMessageBox::warning(this, "Сообщение", "Введите логин");
        return;
    }

    if(!isFilled(ui->loginEdit)) {
        QMessageBox::warning(this, "Сообщение", "Введите пароль");
        return;
    }

    if(isAuthentificated(ui->loginEdit->text(), ui->passwordEdit->text()))  {
        this->hide();
        win = new SPPZWindow(this);
        win->showMaximized();
    }
    else    {
        QMessageBox::warning(this, "Сообщение", "Неправильный логин или пароль");
        return;
    }
}

bool AuthDialog::isFilled(QLineEdit* le) {
    if (le->text().isEmpty() || le->text().isNull())
        return false;
    return true;
}

bool AuthDialog::isAuthentificated(QString login, QString password) {
    bool result = false;
    QSqlQuery query(DataBaseMethods::dataBase());
    query.prepare("SELECT (password = crypt(?, password)) AS pswmatch , group_of_accounts.name "
                      "FROM account_group_of_accounts_binding "
                      "    INNER JOIN group_of_accounts ON account_group_of_accounts_binding.group_of_accounts_id = group_of_accounts.group_of_accounts_id "
                      "    INNER JOIN account ON account_group_of_accounts_binding.account_id = account.account_id "
                      "WHERE login = ?;");
    query.addBindValue(login);
    query.addBindValue(password);
    query.exec();
    QString runned = query.lastQuery();
    if(query.next())    {
        result = query.value(0).toBool();
        AuthDialog::accountGroup = query.value(1).toString();
    }
    return result;
}
