#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QMessageBox>
#include "sppzwindow.h"
#include "databasemethods.h"
#include <QtSql/QSqlQuery>
namespace Ui {
class AuthDialog;
}

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(QWidget *parent = 0);
    ~AuthDialog();
    static QString accountGroup;

private slots:
    void on_enterButton_clicked();

private:
    Ui::AuthDialog *ui;
    bool isFilled(QLineEdit *le);
    QWidget* win;
    bool isAuthentificated(QString login, QString password);
};

#endif // AUTHDIALOG_H
