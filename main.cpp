#include "authdialog.h"
#include <QApplication>
#include "databasemethods.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AuthDialog w;
    w.show();

    DataBaseMethods::initDataBase();
    return a.exec();
}
