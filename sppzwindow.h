#ifndef SPPZWINDOW_H
#define SPPZWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTimer>
#include <QTimerEvent>
#include <QTime>
#include <QMessageBox>
#include "qadvancedtimer.h"
#include "startwizard.h"
namespace Ui {
class SPPZWindow;
}

class SPPZWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SPPZWindow(QWidget *parent = 0);
    ~SPPZWindow();

private slots:
    void on_toolsButton_clicked();

    void on_pauseButton_clicked();

    void on_startButton_clicked();

    void updateTimeLabels();
    void commonTimeIsOut();
private:
    Ui::SPPZWindow *ui;
    void closeEvent(QCloseEvent *event);
    void clear(QWidget *widget);
    void show(QWidget *widget);
    void changePauseIcon();
    QAdvancedTimer commonTime;
    QTimer updateTime;
    void setUpdating();
};

#endif // SPPZWINDOW_H
