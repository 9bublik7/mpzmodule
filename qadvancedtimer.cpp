#include "qadvancedtimer.h"

QAdvancedTimer::QAdvancedTimer():
    QTimer()
{
    curTime = 0;
}

void QAdvancedTimer::pause()
{
    if(!this->isActive()) return;
    curTime = this->remainingTime();
    this->stop();
}

void QAdvancedTimer::resume()
{
    if(curTime==0) return;
    this->start(curTime);
}

// TODO:
//-- трижды высвечивается таймер
