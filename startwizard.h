#ifndef STARTWIZARD_H
#define STARTWIZARD_H

#include <QWizard>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLabel>

class StartWizard : public QWizard
{
    Q_OBJECT
public:
    StartWizard(QWidget *);
    void run();
    void getDisciplineId();
    void getThemePlanId();
    void getSPPZId();
private:
    QWizardPage* createPage();
    QComboBox* disciplineEdit;
    QLabel* disciplineLabel;
    QComboBox* lessonEdit;
    QLabel* lessonLabel;
    QComboBox* groupEdit;
    QLabel* groupLabel;
};

#endif // STARTWIZARD_H
